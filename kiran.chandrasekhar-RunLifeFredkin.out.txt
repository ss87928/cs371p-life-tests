*** Life<FredkinCell> 7x7 ***

Generation = 0, Population = 10.
0-----0
-------
-0-----
0---0--
----0-0
--0-0--
------0

Generation = 3, Population = 27.
-21-10-
2-0--0-
-0-1200
1010--2
-12-1--
--1010-
--0--01

Generation = 6, Population = 14.
1-11-00
-------
03-----
-------
-----02
-------
12-11-2

Generation = 9, Population = 0.
-------
-------
-------
-------
-------
-------
-------

Generation = 12, Population = 0.
-------
-------
-------
-------
-------
-------
-------

Generation = 15, Population = 0.
-------
-------
-------
-------
-------
-------
-------

Generation = 18, Population = 0.
-------
-------
-------
-------
-------
-------
-------

Generation = 21, Population = 0.
-------
-------
-------
-------
-------
-------
-------

Generation = 24, Population = 0.
-------
-------
-------
-------
-------
-------
-------

Generation = 27, Population = 0.
-------
-------
-------
-------
-------
-------
-------

Generation = 30, Population = 0.
-------
-------
-------
-------
-------
-------
-------

Generation = 33, Population = 0.
-------
-------
-------
-------
-------
-------
-------

Generation = 36, Population = 0.
-------
-------
-------
-------
-------
-------
-------

Generation = 39, Population = 0.
-------
-------
-------
-------
-------
-------
-------

Generation = 42, Population = 0.
-------
-------
-------
-------
-------
-------
-------

Generation = 45, Population = 0.
-------
-------
-------
-------
-------
-------
-------

*** Life<FredkinCell> 6x5 ***

Generation = 0, Population = 8.
-0---
0---0
-----
----0
----0
-00-0

Generation = 11, Population = 18.
04135
1----
2201-
4112-
-----
4-523

Generation = 22, Population = 18.
---38
-96--
46-3-
59236
566--
5-+3-

Generation = 33, Population = 14.
19-4+
2+9-+
-----
-+-4+
8----
-+--+

Generation = 44, Population = 7.
-----
3----
-+--8
-+---
+-+--
----+

Generation = 55, Population = 18.
-+-8+
3+-8+
----8
++--+
+++8-
-+-8+

Generation = 66, Population = 15.
-+4++
--+8-
9----
+----
--+9-
+++9+

*** Life<FredkinCell> 5x5 ***

Generation = 0, Population = 7.
--0--
-----
--0--
0-0-0
0---0

Generation = 10, Population = 7.
2---2
2-2-2
--+--
-----
--3--

Generation = 20, Population = 7.
--5--
-----
--+--
5-5-5
5---5

Generation = 30, Population = 7.
7---7
7-7-7
--+--
-----
--8--

Generation = 40, Population = 7.
--+--
-----
--+--
+-+-+
+---+

Generation = 50, Population = 7.
+---+
+-+-+
--+--
-----
--+--

*** Life<FredkinCell> 5x5 ***

Generation = 0, Population = 6.
---0-
0----
-----
0-0-0
--0--

Generation = 5, Population = 13.
00-1-
-----
-0430
3-2-2
40-1-

Generation = 10, Population = 13.
23--1
2-3-2
-39--
6-5-5
-32--

Generation = 15, Population = 13.
-6-53
5-6-5
06+7-
-----
-6-53

Generation = 20, Population = 13.
--08-
8-9-8
--++-
+-9-9
7--85

*** Life<FredkinCell> 8x8 ***

Generation = 0, Population = 11.
--------
--------
00--0---
----0-0-
0-----00
--0-----
--------
-0-0----

Generation = 9, Population = 28.
-201----
-232-0--
24-275--
---0----
1-4--1--
-11----2
320--1-1
011---1-

Generation = 18, Population = 32.
-5--4-42
--4-----
345-+6--
1433--7-
---6413-
14-4--9-
82--4---
-36-2238

Generation = 27, Population = 35.
7-544254
5--+--75
--88+--5
264---+5
3--+7-64
-6---0--
+-----1-
6-9+23-+

Generation = 36, Population = 19.
--------
-9++----
5-+-++--
2-6---+-
4-+-+--5
--------
+5--+---
---+---+

Generation = 45, Population = 31.
++99739-
9+--+---
--+-+-+-
-96--3-9
--+-----
--+-0-++
+-63---+
+6-+3--+

*** Life<FredkinCell> 9x4 ***

Generation = 0, Population = 9.
---0
-00-
--0-
----
0--0
-0--
0---
----
-0--

Generation = 16, Population = 15.
3664
11-0
--53
----
--56
--01
-4--
----
-3--

Generation = 32, Population = 13.
8--9
11-0
-4-5
----
-0-+
--01
--6-
----
--0-

Generation = 48, Population = 17.
-++-
11-0
9-+-
----
0-+-
--01
9+-+
----
++-8

*** Life<FredkinCell> 8x8 ***

Generation = 0, Population = 29.
-0-0-0--
0000000-
--0----0
--000--0
-0-00-0-
-0-00--0
---00--0
0-0-----

Generation = 13, Population = 34.
03--375-
4-+---32
34032--+
--17--33
-206----
-4--6454
12------
36--562-

Generation = 26, Population = 25.
--+-7+9-
6----5-3
57---7-+
2--+-+--
3--+-6--
+----7+-
1--4-4--
--2--+--

Generation = 39, Population = 29.
0-+--+-3
--++-67-
-+-+-+6+
-+7+-++-
---+-9--
++------
-+35--0-
9---+--7
