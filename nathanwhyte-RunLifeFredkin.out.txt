*** Life<FredkinCell> 10x19 ***

Generation = 0, Population = 19.
---0---------------
-------------0-----
-0-----------00----
--0-0----0---------
-------------------
-------------------
---0---00----0-----
-------------------
--------00-------0-
-0---0------0---0--

Generation = 5, Population = 96.
0-0---0-00--0-30-0-
-----0---1--0---01-
0-0-010212212-1-1-1
-0-00-30-00-020---0
-00-0-00-10-0-----0
020---000------0-0-
0-01-01--2-3-1200-0
0-202-00--0---00---
-000-200-211-0----0
-----100--1010-0---

Generation = 10, Population = 80.
-----01----0----0--
---0-1---1-----2031
-2--0----------2--4
-2-2----1--2-311221
1----1--620021-20--
-----2---111---210-
-43---235-24----141
-34--1--2----00---0
-33-----2--1-101220
004-11210-1-1-0----

Generation = 15, Population = 97.
-0-11---5---2-52---
0---06--13---3222--
-34-0316-57--14358-
041--4-13-643--2524
1--2-22-652-3----13
2--2--21---320-2---
2-4522-5-3-5-7---92
269--1--2---01--1-1
-----21-----5302-41
32613------1---121-

Generation = 20, Population = 91.
1---1----2-12-84-2-
-64-3-3-1---3---4--
-45-0-1---7--454-97
-6---45-----432---4
------24---0-------
285--52-522-33--3-1
264832------0955-+2
47--5-4--2---4--13-
--842-2-4+4354---4-
42--514-013-432-32-

*** Life<FredkinCell> 4x12 ***

Generation = 0, Population = 17.
0-----0-0--0
-0---0--0--0
0-0-0----000
0-----00----

Generation = 5, Population = 22.
30--10----05
-2-0201--00-
3--01-140--1
1--1--------

Generation = 10, Population = 27.
---3130011--
---1332-41-6
6201-53623-3
---3--5---3-

Generation = 15, Population = 24.
5013---0--49
38------6-0-
---4-5--342-
---65364-745

Generation = 20, Population = 28.
---664--5--+
-83-66-4--1-
9---585665--
36-858-65867

*** Life<FredkinCell> 18x6 ***

Generation = 0, Population = 18.
----0-
------
-0----
------
--0-0-
-0----
0-----
0-0--0
----0-
------
0--0--
--0---
------
--0---
0-----
------
0-----
---0-0

Generation = 5, Population = 57.
2330--
0---00
---12-
22-001
0000-0
-2-10-
---1--
2----1
2-2-0-
-11001
----2-
203011
100--0
-0-2--
2---20
-03---
-0---0
-1110-

Generation = 10, Population = 54.
--3-02
--10--
026152
223312
2-1101
1--43-
-0----
4-151-
--53--
----02
110--2
3----4
---12-
------
3---5-
115-00
----0-
-2432-

Generation = 15, Population = 45.
---4--
----2-
--62--
-43---
3621-1
-3--5-
-1-4--
451-6-
-1--20
2422-2
1-3333
5---36
-02-4-
--4---
---2-4
--8-3-
---2--
2-----

Generation = 20, Population = 57.
--7-4-
3530--
----+5
-56-55
5-5--3
-3-7--
-2---3
7--79-
-2-33-
46334-
1--66-
-3-34-
312-52
-89-3-
-3-2-4
2--344
213---
--5-31

*** Life<FredkinCell> 18x19 ***

Generation = 0, Population = 20.
--0----------------
-------------------
-------------------
----0----00--------
-0---------0-------
---------0--0-----0
-0--0--0-----------
----------------0--
----------------0--
-------------------
-----0-------------
------0--------0---
------------0------
-------------------
-------------------
-------------------
0------------------
----------0--------

Generation = 5, Population = 147.
110--0-0-00-------0
0-0-1-000-1--0---0-
-31------0--000-0-0
-0----3--2--021010-
-1211-0-0-----01-0-
0-00210-10-1011-0--
23---2202----000--0
0------0-32--01-102
0-0-1-0-----0--0-0-
-0--0--0010--0---02
--00-00--1-00---1-0
00-00--00-210--0-0-
-00----0-0-0----0--
0----0--00-0-------
----0----------0---
0-0-0--0-0-00-0-0--
-----0----00-000---
0-0-00-0----00-0---

Generation = 10, Population = 152.
---0---322121-0-0-1
-0--1-22-2-10-0011-
-5-0-1----0-10--1--
-220--42-5-325212-3
---4-1---41-0-12---
-0123----3------12-
3-1--3--231423---1-
-0--1-2---4-----322
-11-210--11--1-12--
1---2--2-3--1--2---
-0201---1-----01--1
3----111--52-1--101
2--12-13-2------0-2
1---0----022---1--2
111111--11-01---1-1
--0---2--0--2-11--0
-0----201---------1
0-0---3-1----0-1-1-

Generation = 15, Population = 158.
-3-1----3---115-2--
----2--2-5-3--1--40
-5---12-1-0---00-00
2-3--263-8--37-4---
-1-6-2-01--0--1-12-
3-23--3-4-23-4-03-5
-62--3-35----603-1-
-3--4-5--6-102-3-45
-2-0-23----5--223-0
-----21--5411-13134
-1-11-333--2----1-2
50-15---22----6-1-4
3---21231--2-0----2
3-1--------------22
2--244226--0-4---0-
2-302--3022---4-30-
3-40----222-5---2--
103------4-1-1-1---

Generation = 20, Population = 135.
1-0221--4--4215--1-
----445------61----
0------21-1-52-1-0-
-33---9---68--6-51-
--7---10-----2-4--2
62------454-3-20-48
-6-4-36----5-9--635
----53+358---5-35--
-74-6--------4---1-
--2-2---0--1--2-4--
-23-1--4-23--1--3-3
---263--34-6136-4--
--5-2-36---3----2-4
------2---5-2--1---
---2--5-63-------0-
----31-6----525-4--
6----250---38-30---
-----07-56--223---2

*** Life<FredkinCell> 8x6 ***

Generation = 0, Population = 17.
0-0-0-
-0---0
0----0
0-00--
---0--
-0-0--
---00-
0-0---

Generation = 5, Population = 28.
--31--
-00102
32002-
-0214-
4-2--1
1-1--3
02---2
1-0--0

Generation = 10, Population = 24.
---323
4-0-13
-3--43
3132--
--202-
2--00-
-----2
-0-14-

Generation = 15, Population = 21.
-0----
5----8
7-0045
5-5-50
63--2-
2-----
13--33
----5-

Generation = 20, Population = 23.
-----5
----5+
+---7-
5--5--
+5---6
-4800-
260-3-
50-486

*** Life<FredkinCell> 9x12 ***

Generation = 0, Population = 18.
-------0----
---------0--
0-0---------
0-----000---
---------0--
0-------0---
-0-0-00--0--
--0--------0
------------

Generation = 5, Population = 46.
3-1200---2-2
-----10-22-1
--31----02-0
1-----------
---10---00--
00-0-0--01--
-0----1-11--
0----00-1-03
0-0--33001-0

Generation = 10, Population = 58.
---3---0-40-
---1-2323--1
23--4----512
315-13--302-
301-135----3
--21-1-2521-
522---3131--
11-03-344-5-
---11--3-22-

Generation = 15, Population = 51.
--3------71-
8-14---54-04
2--47123---3
5--2-3-15325
--3314-5--33
3442-4------
--2-06-331-3
--5-3---5---
8--42--6----

Generation = 20, Population = 60.
4-6-2-3-3+2-
-44---3--904
-66-7-35664-
8-6-546-63--
544--57+---4
6-----2-96-6
--25-63-524-
55---4--8576
+-16---72-3-

*** Life<FredkinCell> 15x3 ***

Generation = 0, Population = 16.
-0-
-00
---
--0
0--
---
---
---
0--
-00
--0
-00
-0-
-00
00-

Generation = 5, Population = 25.
--0
--2
12-
2--
322
001
---
-3-
32-
031
2--
2--
2-1
--1
111

Generation = 10, Population = 24.
0--
335
14-
-51
--3
-3-
-11
---
-40
-6-
--1
-41
32-
142
2--

Generation = 15, Population = 0.
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---

Generation = 20, Population = 0.
---
---
---
---
---
---
---
---
---
---
---
---
---
---
---
